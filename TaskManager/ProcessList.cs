﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;
using System.Windows.Forms;

namespace TaskManager
{
    //Wrapper class for list of processes
    class ProcessList : INotifyPropertyChanged
    {

        private string filter;

        public string Filter { get { return this.filter; } set { this.filter = value; OnPropertyChanged("Filter"); } }


        private ObservableCollection<MyProcess> processes;

        //Observable for usage within the GUI
        public ObservableCollection<MyProcess> Processes
        {
            get { return processes; }
            set {
                IEnumerable<MyProcess> filtered;
                if(this.Filter != null) {
                    //If the filter is set filter and order the list
                    filtered = value.Where(p => p.Name.Contains(this.Filter)).OrderByDescending(e => e.Id);
                } else {
                    //If filter is not set just order the list
                    filtered = value.OrderByDescending(e => e.Id);
                }

                this.processes = new ObservableCollection<MyProcess>(filtered);
                
                //Notify listeners for immediat update
                OnPropertyChanged("Processes"); }
        }

        public ProcessList()
        {
            processes = new ObservableCollection<MyProcess>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }


        public ICommand Snapshot { get { return new CommandHandler(() => saveSnapshot()); } }

        //Saves the process list as XML
        private void saveSnapshot()
        {
            XmlSerializer writer = new XmlSerializer(processes.GetType());

            FileStream file = MainViewModel.getFileStream("Save Snapshot", "XML|*.xml", "log_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xml");

            if (file != null) {
                writer.Serialize(file, processes);
                file.Close();
            }

            
        }


    }
}
