﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace TaskManager
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

       //private MainViewModel mvm = new MainViewModel();
        MainViewModel mvm;

        public MainWindow()
        {
            InitializeComponent();

            //Nicer would be this.DataContext = new MainViewModel but it does not want to work...
            mvm = new MainViewModel(lblCPU, lblMem, prgCPU, prgMem, lvProc);

            btnStartCP.Click += btnStartCP_Click;
            btnEndCP.Click += btnEndCP_Click;

            btnSnap.DataContext = mvm.Processes;

        }

        Process CPProcess;

        //Ends the consumer process if it is running
        void btnEndCP_Click(object sender, RoutedEventArgs e)
        {
            if (CPProcess != null)
            {
                if(!CPProcess.HasExited)
                    CPProcess.Kill();
                CPProcess = null;
            }
        }

        //Starts the consumer process if it is not already running
        void btnStartCP_Click(object sender, RoutedEventArgs e)
        {
            if (CPProcess == null) {
                CPProcess = new Process();
                CPProcess.StartInfo.FileName = "E:/VS_WS/SomeProcess/SomeProcess/bin/Debug/SomeProcess.exe";
                CPProcess.Start();
                Console.WriteLine("Started {0} ({1})", CPProcess.ProcessName, CPProcess.Id);
            }
        }

        
        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtSearch.Text == null || txtSearch.Text.Equals(""))
            {
                //No filter so we forward null
                mvm.filterProcesses(null);
            }
            else
            {
                //Forward the text for filtering the process list
                mvm.filterProcesses(txtSearch.Text);
            }
        }


        
    }
}
