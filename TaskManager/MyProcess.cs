﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Input;
using System.Xml.Serialization;

namespace TaskManager
{
    //Wrapper class for processes with annotation for XML serialisation
    [XmlType("Process")]
    public class MyProcess
    {
        public MyProcess()
        {

        }


        public MyProcess(Process p)
        {
            this.Proc = p;
        }

        [XmlIgnore]
        public Process Proc { get; set; }
        
        [XmlAttribute("id")]
        public int Id { get { return Proc.Id; } set { } }

        [XmlAttribute("name")]
        public string Name { get { return Proc.ProcessName; } set { } }

        [XmlAttribute("time")]
        public string Time { get { return this.ProcessTime.ToString(); } set { } }

        [XmlAttribute("priority")]
        public string Prio { get { return this.Priority.ToString(); } set { } }


        [XmlIgnore]
        public ProcessPriorityClass Priority { 
            get { 
                try{
                    return Proc.PriorityClass;
                } 
                catch(Exception) {
                    return (ProcessPriorityClass)0;
                } 
            }
        }

        [XmlIgnore]
        public TimeSpan ProcessTime
        {
            get
            {
                try{
                    return this.Proc.TotalProcessorTime;
                }
                catch (Exception)
                {
                    return TimeSpan.Zero;
                } 
            }
        }

        [XmlIgnore]
        public ICommand Kill { get { return new CommandHandler(() => doKill()); } }

        public override string ToString()
        {        
            return this.Proc.ProcessName;
        }


        public void doKill()
        {
            this.Proc.Kill();
        }

    }

    
}
