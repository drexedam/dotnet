﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace TaskManager
{
    //Event args for process list
    public class ProcessListEventArgs
    {

        public MyProcess[] Processes { get; set; }

        public ProcessListEventArgs(Process[] processes)
        {
            Processes = new MyProcess[processes.Length];
            for (int i = 0; i < processes.Length; i++)
            {
                Processes[i] = new MyProcess(processes[i]);
            }
        }
    }
}
