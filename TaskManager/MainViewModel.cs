﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace TaskManager
{
    //Setting this Class/Object as DataContext does not seem to work so we provide a constructor 
    //with all elements or let the user manually bind to the public props
    class MainViewModel
    {
        public SystemPerformanceReader SystemPerformanceReader { get; private set; }

        public ProcessReader ProcReader { get; private set; }

        public PerformanceValues PerformanceValues { get; private set; }
        
        public ProcessList Processes { get; private set; }

        public MainViewModel()
        {
            //Initialisation
            this.ProcReader = new ProcessReader();
            this.SystemPerformanceReader = new SystemPerformanceReader();
            this.PerformanceValues = this.SystemPerformanceReader.Values;
            this.Processes = this.ProcReader.ListOfProcesses;
        }

        public MainViewModel(Label lblCPU, Label lblMem, ProgressBar prgCPU, ProgressBar prgMem, ListView lvProc)
            : this()
        {
            //Set datacontext for forwared GUI elements because it does not work with WPF

            if(lblCPU != null)
                lblCPU.DataContext = PerformanceValues;
            
            if(prgCPU != null)
                prgCPU.DataContext = PerformanceValues;

            if(lblMem != null)
                lblMem.DataContext = PerformanceValues;
            
            if(prgMem != null)
                prgMem.DataContext = PerformanceValues;

            if(lvProc != null)
                lvProc.DataContext = Processes;
        }

        public void filterProcesses(String procName)
        {
            //Set filter for process list
            this.Processes.Filter = procName;
        }

        //Creates a filestream for saving resources
        //Used within saving of processes as XML
        //Displays a SaveFileDialog
        public static FileStream getFileStream(String title, String filter, String defaultFileName)
        {
            Stream res;
            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
            dialog.Filter = filter;
            dialog.Title = title;
            dialog.FileName = defaultFileName;

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if ((res = dialog.OpenFile()) != null)
                {
                    return (System.IO.FileStream)res;
                }
            }

            return null;
        }
    }
}
