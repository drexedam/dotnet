﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;

namespace TaskManager
{
    public delegate void PerformanceValueEventHandler(object sender, PerformanceValueEventArgs args);

    //Reads CPU and memory values
    class SystemPerformanceReader
    {

        private static float MAXRAM = 8067;
        //If Exception here try: CMD as Admin + lodctr /r
        private PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        private PerformanceCounter memCounter = new PerformanceCounter("Memory", "Available MBytes");
    

        private static Timer oneSecTimer = new Timer(1000);

        public event PerformanceValueEventHandler PerformanceValueChanged;

        public PerformanceValues Values = new PerformanceValues();
        
        public SystemPerformanceReader()
        {
            oneSecTimer.Elapsed += oneSecTimer_Elapsed;
            oneSecTimer.Enabled = true;
            OnPerformanceValueChanged(this.cpuCounter.NextValue(), this.memCounter.NextValue());
        }

        protected void OnPerformanceValueChanged(float cpu, float mem)
        {
            if (PerformanceValueChanged != null)
            {
                PerformanceValueChanged(this, new PerformanceValueEventArgs(cpu, mem));
            }
        }


        void oneSecTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Values.CPUValue = this.cpuCounter.NextValue();

            this.Values.MemValue = MAXRAM-this.memCounter.NextValue();
//            OnPerformanceValueChanged(this.cpuCounter.NextValue(), this.memCounter.NextValue());
        }

    }

}
