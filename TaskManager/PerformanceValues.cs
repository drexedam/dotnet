﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager
{
    //Wrapper class for CPU and memory values supporting property changed events
    class PerformanceValues : INotifyPropertyChanged
    {


        public event PropertyChangedEventHandler PropertyChanged;

        private float cpuValue;
        public float CPUValue
        {
            get
            {
                return this.cpuValue;
            }

            set
            {
                this.cpuValue = value;
                OnPropertyChanged("CPUValue");
            }
        }

        private float memValue;
        public float MemValue
        {
            get
            {
                return this.memValue;
            }

            set
            {
                this.memValue = value;
                OnPropertyChanged("MemValue");
            }
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
