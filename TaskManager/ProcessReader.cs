﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.Collections.ObjectModel;

namespace TaskManager
{
    public delegate void ProcessListEventHandler(object sender, ProcessListEventArgs args);
    //Reads and provides a list of processes
    class ProcessReader
    {
        Process[] processes = Process.GetProcesses();

        public event ProcessListEventHandler ProcessListEventHandler;

        private Timer fiveSecondTimer = new Timer(5000);

        public ProcessList ListOfProcesses = new ProcessList();

        public ProcessReader()
        {
            ListOfProcesses.PropertyChanged += ListOfProcesses_PropertyChanged;
            fiveSecondTimer.Elapsed += fiveSecondTimer_Elapsed;
            fiveSecondTimer.Enabled = true;
            OnElapsed();
        }

        void ListOfProcesses_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Filter"))
            {
                //If the filter has changed we do not want to wait for the timer in order to update the list of processes
                OnElapsed();
            }
        }

        void fiveSecondTimer_Elapsed(object sender, ElapsedEventArgs e)
        {

            OnElapsed();
            //OnProcessListChanged();
        }

        //Updates the list of processes
        private void OnElapsed()
        {
            processes = Process.GetProcesses();

            MyProcess[] procs = new MyProcess[processes.Length];
            for (int i = 0; i < processes.Length; i++)
            {
                procs[i] = new MyProcess(processes[i]);
            }

            ListOfProcesses.Processes = new ObservableCollection<MyProcess>(procs);
        }
       

        protected void OnProcessListChanged()
        {
            ProcessListEventHandler(this, new ProcessListEventArgs(processes));
        }

    }
}
