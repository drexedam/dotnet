﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager
{
    //Old Event args class for CPU and memory value
    public class PerformanceValueEventArgs : EventArgs
    {
        public PerformanceValueEventArgs(float cpu, float mem)
        {
            this.CPUValue = cpu;
            this.MemValue = mem;
        }

        public float CPUValue
        {
            get;
            set;
        }

        public float MemValue
        {
            get;
            set;
        }

    }
}
