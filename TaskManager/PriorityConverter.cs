﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Diagnostics;
using System.IO;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;

namespace TaskManager
{
    class PriorityConverter : IValueConverter
    {
        //Converts a priority class to an BitmapImage
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is ProcessPriorityClass))
            {
                return value;
            }

            ProcessPriorityClass ppc = (ProcessPriorityClass)value;

            var stream = new MemoryStream();

            //Save image to stream according to priority class
            switch (ppc)
            {
                case ProcessPriorityClass.AboveNormal:
                    TaskManager.Properties.Resources.AboveNormal.Save(stream, ImageFormat.Png);
                    break;
                case ProcessPriorityClass.BelowNormal:
                    TaskManager.Properties.Resources.BelowNormal.Save(stream, ImageFormat.Png);
                    break;
                case ProcessPriorityClass.High:
                    TaskManager.Properties.Resources.High.Save(stream, ImageFormat.Png);
                    break;
                case ProcessPriorityClass.Idle:
                    TaskManager.Properties.Resources.Idle.Save(stream, ImageFormat.Png);
                    break;
                case ProcessPriorityClass.Normal:
                    TaskManager.Properties.Resources.Normal.Save(stream, ImageFormat.Png);
                    break;
                case ProcessPriorityClass.RealTime:
                    TaskManager.Properties.Resources.RealTime.Save(stream, ImageFormat.Png);
                    break;
                default:
                    return value;
            }

            //Create bitmap from stream
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.StreamSource = stream;
            bitmap.EndInit();

            return bitmap;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //Not needed just return the value
            return value;
        }
    }
}
