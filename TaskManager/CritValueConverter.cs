﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace TaskManager
{
    class CritValueConverter : IMultiValueConverter
    {
        //Returns if the current CPU value is above the value defined with the slider
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Double critVal = (Double)values[0];
            Single curVal = (Single)values[1];

            return critVal <= curVal;
        }

        
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            //We do not need this so just return null
            return null;
        }
    }
}
